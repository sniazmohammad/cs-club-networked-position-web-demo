const express = require('express');
const path = require('path');
const ws = require('ws');

const appService = require('./app');

// Config

const PORT = 3000;

// HTTP

const expressApp = express();

expressApp.use(express.static(path.join(__dirname, 'static')));

const server = expressApp.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`); 
});

// WS

const wss = new ws.WebSocketServer({ server });

wss.on('connection', (socket) => {
    appService.addUser(appService.getNextId(), socket);
});