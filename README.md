# CS Club - Networked Position Web Demo

This repository contains the source code for a web application built for the John F. Kennedy Memorial High School Computer Science Club. It serves as a proof-of-concept for a few concepts related to game networking, specifically position/movement networking.

## Project Structure

`index.js` Contains code to initialise the server. (E.g. intitialise Express, intialise WebSocket server)

`app.js` Contains the server-side logic for this project.

`static/index.html` The HTML file that will be served by the server.

`static/app.js` Contains the client-side logic for this project. Will be loaded by `static/index.html`.

## Usage

This project requires Node to run.

Install the dependencies for this project by running `npm install` in the root of the project.

Then, start the server using `npm start`.

The server will be listening locally on port 3000, unless the port was reconfigured. Accessing `http://127.0.0.1:3000` using a browser will lead you to the application.

## Concepts

This application uses a client-server model for its networking architecture, where the server is the sole authority.

![Client-Server Model Diagram](/static/client-server-model-diagram.png)

Because the server is the sole authority, the client will send commands to the server, usually in response to user input, and await a response from a server containing the new state of the application and any further instructions for the client.

If the server was not the sole authority, and partial authority was potentially granted to the client. The client will directly calculate the new state of the application (or only whatever the client owns) and sends that state to the server, which will then be broadcasted to other users. This is another approach, but users may modify the client-side application to send manipulated state updates to the server.

_To be written_