const users = {};
const sockets = {};

let lastAssignedId = 0;

function getNextId() {
    lastAssignedId += 1;
    return lastAssignedId;
}

function log(str) {
    console.log(`App: ${str}`);
}

function moveCommand(id, direction) {
    let deltaX = 0;
    let deltaY = 0;

    if (direction == 0) {
        deltaY -= 1;
    }

    if (direction == 1) {
        deltaY += 1;
    }

    if (direction == 2) {
        deltaX -= 1;
    }

    if (direction == 3) {
        deltaX += 1;
    }

    users[id].x += deltaX;
    users[id].y += deltaY;

    broadcastAppState();
}

function addUser(id, socket) {
    users[id] = {
        x: 0,
        y: 0
    };
    sockets[id] = socket;

    socket.on('message', (message) => {
        onMessage(id, message);
    });

    socket.on('close', () => {
        removeUser(id);
    });

    log(`Added user ${id}`);

    broadcastAppState();
}

function removeUser(id) {
    delete users[id];
    log(`Removed user ${id}`);
    broadcastAppState();
}

function onMessage(id, payloadString) {
    const payload = JSON.parse(payloadString);
    moveCommand(id, payload.direction);
}

function broadcastAppState() {
    Object.keys(users).forEach((id) => {
        const payload = JSON.stringify({
            users,
            id
        });
        sockets[id].send(payload);
    });
}

module.exports = {
    addUser,
    getNextId
};