let ws;
let appState = {};
let inputState = {
    0: false,
    1: false,
    2: false,
    3: false
};

// DOM References

const canvas = document.querySelector('#app');
const logEl = document.querySelector('#log');

const renderContext = canvas.getContext('2d');

function log(data) {
    const el = document.createElement('div');
    el.innerHTML = data;
    logEl.appendChild(el);
}

// App logic

function start() {
    setupInput();
    window.requestAnimationFrame(update);
}

function update() {
    renderContext.clearRect(0, 0, canvas.width, canvas.height);

    renderContext.fillStyle = 'rgb(0, 0, 0)';

    Object.keys(appState.users).forEach((userId) => {
        if (userId == appState.id)
            renderContext.fillStyle = 'rgb(255, 0, 0)';
        else
            renderContext.fillStyle = 'rgb(0, 0, 0)';
        renderContext.fillRect(appState.users[userId].x, appState.users[userId].y, 3, 3);
    });

    if (inputState[0])
        sendInputCommand(0);

    if (inputState[1])
        sendInputCommand(1);

    if (inputState[2])
        sendInputCommand(2);
    
    if (inputState[3])
        sendInputCommand(3);

    window.requestAnimationFrame(update);
}

function setupInput() {
    document.addEventListener('keydown', (event) => {
        if (event.key == 'w')
            inputState[0] = true;

        if (event.key == 's')
            inputState[1] = true;

        if (event.key == 'a')
            inputState[2] = true;

        if (event.key == 'd')
            inputState[3] = true;
    });

    document.addEventListener('keyup', (event) => {
        if (event.key == 'w')
            inputState[0] = false;

        if (event.key == 's')
            inputState[1] = false;

        if (event.key == 'a')
            inputState[2] = false;

        if (event.key == 'd')
            inputState[3] = false;
    });
}

// Networking

function sendInputCommand(direction) {
    ws.send(JSON.stringify({ direction }));
}

function onConnected() {
    log('Connected to app service');
}

function onMessage(message) {
    const payload = JSON.parse(message.data);

    if (!appState.id || payload.id != appState.id) {
        log(`Registered as user ${payload.id}`);
        start();
    }

    appState = payload;
}

function onDisconnected() {
    log('Disconnected from app service');
}

function connectToAppService() {
    try {
        ws = new WebSocket(`wss://${window.location.host}`);
        ws.onopen = onConnected;
        ws.onmessage = onMessage;
        ws.onclose = onDisconnected;
    } catch (err) {
        log(err);
        alert(err);
    }
}

connectToAppService();